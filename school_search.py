
import csv
import itertools
import time

TF_IDF = {}
SCHOOL_WEIGHT = 0.5
CITY_WEIGHT = 0.3
STATE_WEIGHT = 0.2


def get_cleaned_tokens_from_word(word):
    word = word.lower()
    symbols = "-!\"#$%&()*+-./:;<=>?@[\]^_`{|}~\n"
    for symbol in symbols:
        word = word.replace(symbol, ' ')
    # only one stop word for the ques so removing it here only
    word = word.replace('school', ' ')
    tokens = word.split()
    return tokens
       
def get_docs_content_from_csv():
    processed_tokens = []
    id_to_school_data = {}
    total_docs = 0
    with open('school_data.csv', mode='r', encoding='utf8', errors='ignore') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            doc_id = row.get('NCESSCH')
            id_to_school_data[row.get('NCESSCH')] = [row['SCHNAM05'], row['LCITY05'], row['LSTATE05']]
            school_tokens_list = get_cleaned_tokens_from_word(row.get('SCHNAM05'))
            city_name_tokens_list = get_cleaned_tokens_from_word(row.get('LCITY05'))
            state_abb_tokens_list = get_cleaned_tokens_from_word(row.get('LSTATE05'))
            processed_tokens += school_tokens_list + city_name_tokens_list + state_abb_tokens_list
            total_docs +=1
    return id_to_school_data, processed_tokens, total_docs


def compute_idf_for_tokens(processed_tokens, total_docs):
    document_frequency = {}
    inverse_document_frequency = {}
    for i in range(len(processed_tokens)):
        token = processed_tokens[i]
        if token in document_frequency:
            document_frequency[token].add(i)
        else:
            document_frequency[token] = {i}    
    for token in document_frequency:
        # not using log here because of the constraint of not using math library
        inverse_document_frequency[token] = total_docs/len(document_frequency[token]) 
    return inverse_document_frequency

def compute_tfidf_for_tokens(id_to_school_data, inverse_document_frequency):
    for doc_id, value in id_to_school_data.items():
        school_tokens_list = get_cleaned_tokens_from_word(value[0])
        city_name_tokens_list = get_cleaned_tokens_from_word(value[1])
        state_abb_tokens_list = get_cleaned_tokens_from_word(value[2])
        doc_tokens = school_tokens_list + city_name_tokens_list + state_abb_tokens_list
        for token in doc_tokens:
            tf_idf_school = tf_idf_state = tf_idf_city = 0
            if token in school_tokens_list:
                tf_idf_school = (doc_tokens.count(token)/len(doc_tokens)) * inverse_document_frequency[token] * SCHOOL_WEIGHT
            if token in city_name_tokens_list:
                tf_idf_city = (doc_tokens.count(token)/len(doc_tokens)) * inverse_document_frequency[token] * CITY_WEIGHT
            if token in state_abb_tokens_list:
                tf_idf_state = (doc_tokens.count(token)/len(doc_tokens)) * inverse_document_frequency[token] * STATE_WEIGHT
            
            if token in TF_IDF:
                TF_IDF[token][doc_id] = (tf_idf_school + tf_idf_state + tf_idf_city)
            else:
                TF_IDF[token] = {}
                TF_IDF[token][doc_id] = (tf_idf_school + tf_idf_state + tf_idf_city)

def search_schools(query):
    tokens = get_cleaned_tokens_from_word(query)
    docs_with_tokens = []

    for token in tokens:
        docs_with_tokens.append(TF_IDF.get(token))    
    # get the docs where all the query tokens exist
    docs_with_all_tokens = set.intersection(*map(set, docs_with_tokens))
    intersection_dict = {key: sum(doc[key] for doc in docs_with_tokens) for key in docs_with_all_tokens}
    # form the results with them sorting by the score
    search_results = sorted(intersection_dict.items(), key=lambda x: x[1], reverse=True)
    
    # if search results more than or equal to 3(question constraint) then return from here.
    if len(search_results) >=3:
        return search_results[:3]    
    # sum the ranking of the leftover docs
    union_dict = {}
    for token_ranking_list in docs_with_tokens:
        for key in token_ranking_list:
            if key in docs_with_all_tokens:
                continue
            if key in union_dict:
                union_dict[key]+= token_ranking_list[key]
            else:
                union_dict[key] = token_ranking_list[key]

    # get the maximum score docs
    union_search_results = sorted(union_dict.items(), key=lambda x: x[1], reverse=True)        
    if union_search_results:
        for i in range(3-len(search_results)):
            search_results.append(union_search_results[i])
    return search_results


if __name__ == '__main__':
    id_to_school_data, processed_tokens, total_docs  = get_docs_content_from_csv()
    inverse_document_frequency = compute_idf_for_tokens(processed_tokens, total_docs)
    compute_tfidf_for_tokens(id_to_school_data, inverse_document_frequency)
    queries = ['elementary school highland park', 'jefferson belleville', 'riverside school 44',
               'granada charter school', 'foley high alabama', 'KUSKOKWIM',]
    for query in queries:
        start_time = time.time()
        search_results = search_schools(query)
        end_time = time.time()
        query_time = round((end_time-start_time), 3)
        print(f'Results for "{query}" (search took: {query_time}s)')
        for index,result in enumerate(search_results, 1):
            print(f'{index}.{id_to_school_data[result[0]][0]}')
            print(f'{id_to_school_data[result[0]][1]}, {id_to_school_data[result[0]][2]}')
        
