
import csv

def print_counts():
    state_dict = {}
    metro_locale_dict = {}
    city_school_dict = {}
    with open('school_data.csv', mode='r', encoding='utf8', errors='ignore') as csv_file:
        csv_reader = csv.DictReader(csv_file)        
        for row in csv_reader:
            if row['LSTATE05'] in state_dict:
                state_dict[row['LSTATE05']].append(row['SCHNAM05'])
            else:
                state_dict[row['LSTATE05']] = []
                state_dict[row['LSTATE05']].append(row['SCHNAM05'])
            if row['MLOCALE'] in metro_locale_dict:    
                metro_locale_dict[row['MLOCALE']] +=1
            else:
                metro_locale_dict[row['MLOCALE']] = 1
            
            if row['LCITY05'] in city_school_dict:
                city_school_dict[row['LCITY05']] +=1
            else:
                city_school_dict[row['LCITY05']] = 1

    total_schools = 0
    for key, value in state_dict.items():
        total_schools+= len(value)
    
    print(f'Total Schools: {total_schools}')
    print('Schools by State:')
    for key, value in state_dict.items():
        print(key, ':', len(value))

    print('Schools by Metro-centric locale:')
    sorted_keys = sorted(metro_locale_dict.keys())
    for key in sorted_keys:
        print(key, ':', metro_locale_dict[key])
    city = max(city_school_dict, key=city_school_dict.get)
    print(f'City with most schools: {city} ({city_school_dict[city]} schools)')
    print(f'Unique cities with at least one school: {len(city_school_dict)}')


if __name__ == '__main__':
    print_counts()
